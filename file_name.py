import mimetypes
import unicodedata
import string

# mimetypes.guess_extension(mimetype) sometimes does not return the optimal file extension for certain
# MIME types. For example ".ksh" is a valid extension for the MIME type "text/plain", but ".txt" would
# be far more common. So we add the key ".ksh" with the value ".txt" to use ".txt" in this case.
MIME_TYPE_CORRECTIONS = {
    '.jpe': '.jpg',
    '.xlb': '.xls',
    '.pwz': '.ppt',
    '.ksh': '.txt'
}

VALID_FILENAME_CHARS = "-_.() %s%s" % (string.ascii_letters, string.digits)


def get_file_extension(resource, published):
    if 'file_extension' in resource:  # All customer databases with Nice version >= v2.8
        file_extension = resource['file_extension_published'] if published else resource['file_extension']
        if file_extension is not None:
            file_extension = '.' + file_extension
    else:  # For versions older than Nice v2.8. Can be removed, once all customers are >= 2.8
        mimetype = resource['mime_type_published'] if published else resource['mime_type']
        file_extension = mimetypes.guess_extension(mimetype)
        if file_extension in MIME_TYPE_CORRECTIONS:
            file_extension = MIME_TYPE_CORRECTIONS[file_extension]

    return file_extension if file_extension is not None else ''


# https://gist.github.com/wassname/1393c4a57cfcbf03641dbc31886123b8
def clean_filename(filename):
    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore').decode()

    # keep only whitelisted chars
    return ''.join(c for c in cleaned_filename if c in VALID_FILENAME_CHARS)
