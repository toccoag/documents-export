#!/usr/bin/python
from configparser import ConfigParser


def config(filename='database.ini', section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)

    if parser.has_section(section):
        return dict(parser.items(section))
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
