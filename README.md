# Documents Export

This Python script can be used to export all documents from a customer Nice database.

## Instructions

Install the required dependencies:

```
pip install -r requirements.txt

```

Create a file `database.ini` in the root folder and set the ${...} placeholders accordingly:
```
[postgresql]
host=${HOST}
database=${DATABASE_NAME}
user=${USERNAME}
```

Run the export:
```
python export.py
```

Once the export is complete, you'll find all documents in the `out` directory. The directory structure of the DMS
is preserved.

Please note:
* You'll find some of the documents in two versions. That's because every document has an unpublished binary document
  and can optionally have a published binary document (unless the publish status of the document is `offline`). We're
  exporting the published binary if it's not the same as the unpublished binary . The published binary documents are
  marked with the suffix "*(published)*".
* All entity documents will be located in the folder `out/Entity documents`. All other folders in the root folder
  represent a DMS or CMS domain.
* In some cases, documents "lost" their parent (because of some bugs in the DMS module which might be fixed now).
  As a result they couldn't be found in the DMS anymore. However, they're still in the database and this exporter
  will export them. You'll find those documents in `out/unknown_location`.


