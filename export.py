#!/usr/bin/python
import psycopg2
import psycopg2.extras
import os
import file_name
from datetime import datetime
from progress.bar import Bar
from config import config
from file_size import sizeof_fmt

DIRECTORY_PATH = 'out'
UNKNOWN_LOCATION_PATH = os.path.join(DIRECTORY_PATH, 'unknown_location')

DOMAINS_QUERY = "SELECT * FROM nice_domain ORDER BY label"

FOLDERS_QUERY = """
    WITH RECURSIVE next_in_line AS (
      SELECT 1 AS depth, array[pk] AS path, *
      FROM nice_folder
      WHERE fk_folder IS NULL
      UNION ALL
      SELECT n.depth + 1, n.path || f.fk_folder, f.*
      FROM nice_folder f
      INNER JOIN next_in_line n ON n.pk = f.fk_folder
    )
    SELECT *
    FROM next_in_line
    WHERE pk IN (SELECT fk_folder FROM nice_folder UNION SELECT fk_folder FROM nice_resource)
    ORDER BY path, name
"""

RESOURCE_COLUMNS = [
    'r.pk', 'r.name', 'r.field_model', 'r.fk_domain', 'r.fk_folder',
    'b.data', 'b.size', 'bp.data AS data_published', 'bp.size AS size_published'
]

FILE_EXTENSION_COLUMNS = [
    'b.file_extension', 'bp.file_extension AS file_extension_published'
]

MIME_TYPE_COLUMNS = [
    'b.mime_type', 'bp.mime_type AS mime_type_published'
]

RESOURCES_QUERY = """
    SELECT %s
    FROM nice_resource r
    INNER JOIN nice_resource_content c ON r.fk_content = c.pk
    INNER JOIN _nice_binary b ON c.data = b.hash
    LEFT OUTER JOIN nice_resource_content cp ON r.fk_content_published = cp.pk
    LEFT OUTER JOIN _nice_binary bp ON cp.data = bp.hash
"""


def export():
    print("Reading config from database.ini")
    params = config()
    print("Connecting to database %s on host %s with user %s"
          % (params.get("database"), params.get("host"), params.get("user")))

    with psycopg2.connect(**params) as conn, conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
        start = datetime.now()

        domain_paths = get_domain_paths(cur)
        folder_paths = get_folder_paths(cur, domain_paths)
        count, size = export_resources(cur, domain_paths, folder_paths)

        end = datetime.now()
        print("%s documents (%s) exported in %s" % (count, sizeof_fmt(size), end - start))


def get_domain_paths(cur):
    print("Reading domains from database")
    cur.execute(DOMAINS_QUERY)

    domain_paths = {}

    for domain in iter(lambda: cur.fetchone(), None):
        domain_name = get_domain_name(domain)
        full_path = os.path.join(DIRECTORY_PATH, domain_name)
        domain_paths[domain['pk']] = full_path

    return domain_paths


def get_domain_name(domain):
    if domain['system']:
        return 'Entity documents'
    else:
        return file_name.clean_filename(domain['label'])


def get_folder_paths(cur, domain_paths):
    print("Reading folders from database")
    cur.execute(FOLDERS_QUERY)

    folder_paths = {}

    for folder in iter(lambda: cur.fetchone(), None):
        folder_paths[folder['pk']] = get_folder_path(folder, domain_paths, folder_paths)

    return folder_paths


def get_folder_path(folder, domain_paths, folder_paths):
    parent_path = get_folder_parent_path(folder, domain_paths, folder_paths)
    dirname = get_directory_name(folder)
    return os.path.join(parent_path, dirname)


def get_folder_parent_path(folder, domain_paths, folder_paths):
    if folder['fk_domain'] is not None:
        return domain_paths[folder['fk_domain']]
    else:
        return folder_paths[folder['fk_folder']]


def get_directory_name(folder):
    if folder['entity_model'] is None or folder['system'] == False:
        return folder['name']
    elif folder['fk_domain'] is not None:
        return folder['entity_model']
    else:
        fk_name = 'fk_%s_entitydocs' % folder['entity_model'].lower()
        return str(folder[fk_name])


def export_resources(cur, domain_paths, folder_paths):
    print("Reading resources from database")

    fetch_resources(cur)

    count_total = cur.rowcount
    count_exported = 0

    total_size = 0

    bar = Bar('Exporting', max=count_total, suffix='%(index)d/%(max)d - %(percent).1f%% - %(eta)ds')

    for resource in iter(lambda: cur.fetchone(), None):
        parent_path = create_folders(resource, domain_paths, folder_paths)

        resource_name = get_resource_name(resource, published=False)
        resource_path = os.path.join(parent_path, resource_name)
        export_object(cur, resource['data'], resource_path)
        total_size += resource['size']

        if resource['data_published'] is not None and resource['data_published'] != resource['data']:
            published_resource_name = get_resource_name(resource, published=True)
            published_resource_path = os.path.join(parent_path, published_resource_name)
            export_object(cur, resource['data_published'], published_resource_path)
            total_size += resource['size_published']

        count_exported = count_exported + 1
        bar.next()

    bar.finish()
    return count_total, total_size


def fetch_resources(cur):
    query = build_resources_query(cur)
    cur.execute(query)


def build_resources_query(cur):
    binary_columns = get_binary_columns(cur)
    all_columns = RESOURCE_COLUMNS + binary_columns
    return RESOURCES_QUERY % ', '.join(all_columns)


def get_binary_columns(cur):
    if column_exists(cur, '_nice_binary', 'file_extension'):
        return FILE_EXTENSION_COLUMNS
    else:  # For versions older than Nice v2.8. This whole workaround can be removed, once all customers are >= 2.8
        return MIME_TYPE_COLUMNS


def create_folders(resource, domain_paths, folder_paths):
    folders_path = get_resource_parent_path(resource, domain_paths, folder_paths)
    os.makedirs(folders_path, exist_ok=True)
    return folders_path


def get_resource_parent_path(resource, domain_paths, folder_paths):
    if resource['fk_folder'] is not None and resource['fk_folder'] in folder_paths:
        return folder_paths[resource['fk_folder']]
    elif resource['fk_domain'] is not None and resource['fk_domain'] in domain_paths:
        return domain_paths[resource['fk_domain']]

    return UNKNOWN_LOCATION_PATH


def get_resource_name(resource, *, published):
    name, ext = os.path.splitext(resource['name'])

    if ext is None or ext == '':
        ext = file_name.get_file_extension(resource, published)

    full_name = name

    if published:
        full_name += '(published)'

    full_name += ext

    return full_name


def export_object(cur, data, path):
    if not os.path.exists(path):
        cur.connection.lobject(data).export(path)


def column_exists(cur, table, column):
    query = """
      SELECT EXISTS (
          SELECT 1 FROM information_schema.columns
          WHERE table_name = '%s'
          AND column_name= '%s'
      )
    """ % (table, column)
    cur.execute(query)
    result = cur.fetchone()
    return result[0]


if __name__ == '__main__':
    export()
